# Lichtblick

<audio controls="">
 <source src="https://7.5bits.winniehell.de/projekttxt/lichtblick/lichtblick.ogg" type="audio/ogg; codecs=vorbis">
 <source src="https://7.5bits.winniehell.de/projekttxt/lichtblick/lichtblick.mp3" type="audio/mpeg">
</audio>

Das schrille Lachen von *219-3a* prallte bedrohlich von den Wänden des Gemeinschaftsraums ab und schien *375b-18* einzukreisen.
Sie blickte sich ängstlich um.
Doch die Arbeiterinnen an den Nachbartischen starrten demonstrativ woanders hin oder fingen schnell eine Unterhaltung an.
Lautes Lachen im Gemeinschaftsraum war sicherlich gegen die Vorschriften und niemand wollte damit in Verbindung gebracht werden.
*375b-18* vergewisserte sich noch, dass auch keiner der Wächter auf sie aufmerksam geworden war.
Dann blickte sie zurück in *219-3a*s grinsendes Gesicht.

- „Du hast Angst vor einer Blume?“, fragte *219-3a* ungläubig.
- „Pscht! Nicht, dass uns jemand hört!“
- „Ach, sollen sie doch kommen und uns verhaften, weil wir über Pflanzen reden.
  Dieses System ist kaputt—merkst du das nicht?“
- „Ich möchte keinen Ärger bekommen.
  Diese Blume hat bestimmt jemand ohne Erlaubnis dort gepflanzt.“
- „Das ist doch lächerlich.
  Wen stört sie denn?
  Das schlimmste Verbrechen dieser Blume ist es, ein wenig Schönheit in eine Welt zu bringen, die nur aus Matsch und Beton besteht.
  Ich verrate dir jetzt etwas: Es ist nicht die einzige.
  Im ganzen Industrieviertel sind solche Blumen aufgetaucht... und mir gefallen sie.“

Eine Weile herrschte Stille.
Dann fragte *375b-18* mit besorgter Mine und noch leiser als vorher:

- „Du meinst, es handelt sich um eine Rebellion? Stecken Arbeiterinnen dahinter?“
- „Schon möglich.
  Ich habe etwas gehört, aber genaueres weiß ich auch nicht.
  Es ist noch zu gefährlich offen darüber zu reden.
  Ich denke, den meisten Arbeiterinnen ist klar, dass sich etwas ändern muss.“

Auf dem Weg zurück in ihren Wohnblock war *375b-18* sehr nachdenklich.
Hatte eine Rebellion der Arbeiterinnen einen Sinn?
Würde das nicht in noch größeren Schikanen durch die Wächter enden?
Der Gedanke an die kleine Blume, die sie am nächsten Tag auf dem Weg zu ihrer Arbeit sehen würde, heiterte sie wieder auf.
Vielleicht war die Knospe ja sogar am Wochenende ein wenig gewachsen...
