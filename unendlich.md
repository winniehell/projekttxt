<center>
  <img src="unendlich.png" />
</center>

# Unendlich

Im Industrieviertel angekommen, sah *375b-18* schon von Weitem die endlosen Warteschlangen der Arbeiterinnen vor den Fabrikgebäuden.
Als sich die Türen des Werksbus zischend öffneten, hatte sie den Eindruck, in einer Welle aus grauer Arbeitskleidung zu ertrinken.
Alle strömten auf die Eingangstore zu, an denen Wächter routinemäßig auf verbotene Gegenstände überprüften.
Natürlich hatte es schon seit Jahren keinen Zwischenfall mehr gegeben, aber die Vorschrift wollte es so.
*375b-18* war sich sicher, dass es subtilere Wege gab, Dinge unbemerkt in die oder aus der Fabrik zu schmuggeln.

Dort, wo die Sonne langsam über den Horizont kletterte, waren die Gebäude von Block 2 zu erkennen.
Zumindest glaubte *375b-18* das.
Es war schwer zu erkennen – nicht nur weil die aufgehende Sonne alles in einen grauroten Brei verwandelte, sondern auch weil vor jedem grauen Klotz im Brei Schlangen von Arbeiterinnen in derselben Arbeitskleidung standen und darauf warteten mit der Arbeit zu beginnen.
Irgendwo da hinten war auch *219-3a*.
Früher einmal stand sie in der gleichen Schlange wie *375b-18*.
Damals war ihre ID noch *375b-17*.
Doch dann wurde sie nach Block 2 versetzt.
*375b-18* wusste nicht warum, denn es war verboten darüber zu reden.
Es hatte aber sicherlich mit dem Verstoß gegen irgendeine Vorschrift zu tun – soviel stand fest.

Plötzlich nahm *375b-18* aus dem Augenwinkel etwas Grünes auf dem schlammigen Boden wahr.
Die anderen Arbeiterinnen schlurften gleichgültig daran vorbei.
Doch sie senkte den Kopf, um es sich genauer anzusehen.
Zwischen ein wenig aufgewühlter Erde wuchs etwas.
Es war kein Moos und auch kein Gras: Es hatte schmale, ovale Blätter.
*375b-18* hockte sich hin und erkannte, dass schützend zwischen den Blättern eine kleine Knospe wuchs.
Schnell stand sie wieder auf und eilte den anderen Arbeiterinnen hinterher.
Sie wollte nicht die Aufmerksamkeit der Wächter auf sich lenken.
Pflanzen waren bestimmt gegen die Vorschriften und diese sah so aus, als ob sie dort absichtlich hingesetzt worden war.

Den Rest des Tages musste *375b-18* während ihrer monotonen Arbeit immer wieder an diese kleine Pflanze denken.
Sie freute sich schon auf den nächsten Morgen, wenn sie dort wieder vorbeikommen würde.
Das war etwas, wovon sie *219-3a* unbedingt am Wochenende erzählen musste.
Wochenende... die fünf Arbeitstage, die noch vor ihr lagen, kamen *375b-18* unendlich lang vor.

