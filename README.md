# Projekt [@winniehell].txt

Dies sind meine Beiträge zu [Projekt *.txt].
Texte, Bilder und Musik sind unter [CC BY-NC-SA 4.0].

---

These are my entries for [Projekt *.txt].
They are all in German.
That is your chance to start a German course. 😃

---

[![banner](banner.jpg)][Projekt *.txt]

[@winniehell]: https://winniehell.de
[Projekt *.txt]: http://projekttxt.net/
[CC BY-NC-SA 4.0]: https://creativecommons.org/licenses/by-nc-sa/4.0/
