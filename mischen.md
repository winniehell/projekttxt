# Mischen

Es war wieder Montagmorgen und schon beim ersten Morgenalarm sprang _357b-18_ aus dem Bett und zog sich an.
Das Frühstück schlang sie hastig herunter und kurz darauf schloss sich die Wohnungstür hinter ihr.
Statt zu warten bis der Aufzug kam, sprang _357b-18_ die Treppenstufen herunter und kam noch rechtzeitig für den ersten Werksbus unten an.
Ungeduldig zählte sie die grauen Wohntürme an denen sie vorbei fuhren und ihre Mine erhellte sich als sie das Industrieviertel erreichten.

Kaum öffneten sich zischend die Türen vom Bus, streckte _357b-18_ schon den Fuß nach draußen.
Seit _219-3a_ den Block gewechselt hatte, war sie nur widerwillig zur Arbeit gegangen.
Doch das hatte sich jetzt geändert.
Die Blume war in der letzten Woche ein gutes Stück größer geworden und jetzt bestimmt schon... weg?

_357b-18_ sah sich verzweifelt suchend um.
Nichts als braune Erde.
Nicht einmal das kleinste Anzeichen, dass hier je etwas gewachsen war.
Sie hockte sich hin um den Boden genauer zu untersuchen.
Doch außer Fußspuren konnte sie nichts entdecken.

„Hast du etwas verloren?“
_357b-18_ zuckte zusammen als sie die Stimme des Wächters hörte.
Langsam stand sie auf und drehte sich zu ihm um.
„Ja, den Ring den mir meine Mutter schenkte“
Hastig fügte sie hinzu: „Aber ich muss ihn wohl an einem anderen Ort verloren haben.“
„Wir können einen Sucher anfordern.“
„Vielen Dank, aber ich gucke lieber vorher noch einmal in meiner Wohnung nach.“

Schnell reihte sich _357b-18_ wieder in die Schlange der Arbeiterinnen ein und wartete nervös auf die Kontrolle am Eingang.
Der Wächter verharrte noch eine Weile und beobachtete sie.
Dann kehrte er auf seinen Posten zurück.
_357b-18_ war ein wenig erleichtert.
Ob er wohl Verdacht geschöpft hatte?
War den Wächtern die Blume etwa auch aufgefallen?
Hatten SIE die Pflanze gestohlen?

Mechanisch und lustlos erledigte _357b-18_ ihre Arbeit.
Zwischendrin sah sie immer wieder besorgt zu den Wächtern und brach jedes Mal in stille Panik aus, wenn einer von ihnen sich in ihre Richtung bewegte.
Dann endlich konnte sie wieder in den Werksbus einsteigen.

In den darauffolgenden Tagen wich ihre Niedergeschlagenheit allmählich Zorn.
Jemand musste doch dafür verantwortlich sein, dass diese Blume verschwunden war.
Sie konnte sich schließlich nicht einfach aufgelöst haben.
Hatten die Wächter sie rausgerissen, weil sie dort nicht hingehörte?
War es tatsächlich gegen die Vorschriften, wenn irgendwo kleine Blumen wuchsen?

_357b-18_ musste daran denken, was _219-3a_ bei ihrer letzten Begegnung am Wochenende auf dem Rückweg vom Gemeinschaftsraum erzählt hatte.
Der Vater von _219-3a_ s Sohn war ein Privi und darum hatte er das Kind zu sich genommen.
Es sollte als Privi aufwachsen.
_219-3a_ wollte ihr Kind besuchen, doch ihr wurde nachdrücklich versichert, dass es unmöglich wäre.
Privis und Arbeiterinnen dürfen sich nicht mischen.
_219-3a_ hatte ihren Sohn trotzdem besucht – heimlich.
Als die Wächter davon erfuhren, wurde _219-3a_ zur Strafe nach Block 2 versetzt.

_357b-18_ schnaubte und spuckte verächtlich auf den matschigen Boden.
Etwas musste sich ändern.
