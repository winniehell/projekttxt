# Anfang

<audio controls="">
 <source src="https://7.5bits.winniehell.de/projekttxt/anfang/anfang.ogg" type="audio/ogg; codecs=vorbis">
 <source src="https://7.5bits.winniehell.de/projekttxt/anfang/anfang.mp3" type="audio/mpeg">
</audio>

Die Regentropfen pressten sich gegen die Fensterscheibe – fast als wollten sie auch den gräulichen Wolken entkommen.
*357b-18* saß mittlerweile aufrecht im Bett und schaute dem Trübsal in die Augen, das wohl entfernt verwandt mit Wetter war.
Sie dachte an all die Privis in ihren Luxuswohnungen, die sich Visualisierungen für ihre Fenster leisten konnten.
Bei denen war jetzt sicherlich strahlender Sonnenschein vor türkisblauem Meer.
Ob sie wohl überhaupt von dem Dauerregen wussten, der durch die Abgase der Industrieviertel entstand?
Bestimmt berührten deren glänzend weiße Schuhe nie die schlammigen Straßen zwischen den Wohntürmen.

Als der zweite Morgenalarm durch die Wohnung dröhnte, schreckte *357b-18* aus ihren Träumen auf.
Sie sprang aus dem Bett und lief zur Küchenzeile.
Eine letzte Scheibe Toastbrot bettelte stumm darum nicht verspeist zu werden.
Vergeblich.
Zusammen mit einigen Bröckchen Ersatzkäse, wurde es durch den Inhalt der Wasserflasche die Speiseröhre von *357b-18* heruntergespült.
*Einkaufen!*, notierte sich *357b-18* auf ihrem rosafarbenen, gedanklichen Notizblock.
Eigentlich war sie sich nicht so sicher, ob er wirklich rosafarben war.
*357b-18* hatte von der Farbe gehört, aber sie noch nie gesehen.
*Wie pink, aber nicht so knallig, sondern sanft*, hatte ihre Freundin *219-3a* es beschrieben.
*So wie Rosen eben.*
Auch eine Rose hatte *357b-18* noch nie gesehen und sie zweifelte ein wenig daran, dass es *219-3a* anders erging.
Zwischen den Wohntürmen und in den Industrievierteln war man froh, wenn man mal ein Stückchen Moos zu gesicht bekam.
Blumen wuchsen dort ganz sicher nicht.

Hastig streifte *357b-18* sich die Arbeitskleidung über und verlies die Wohnung.
Weil sie so spät dran war, hatte sie den Aufzug für sich.
Unten konnte sie gerade noch in den Werksbus springen, bevor die Türen schlossen.
So fing die Woche an...
